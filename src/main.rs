//! CLI tool to generate structs in different languages from specially
//! crafted TOML files. The format of these files follows the following
//! rules.
use std::io::Write as IoWrite;
use std::{fs::File, io::Read, path::PathBuf};

mod c_lang;
mod rs_lang;

use clap::Parser;

#[derive(serde::Deserialize)]
struct StructDef {
    name: String,
    arch: Option<Vec<String>>,
    description: String,
    fields: Vec<FieldDef>,
}

#[derive(serde::Deserialize)]
struct FieldDef {
    name: String,
    arch: Option<Vec<String>>,
    array_len: Option<usize>,
    description: String,
    typ: String,
}

#[derive(serde::Deserialize)]
struct FileDef {
    name: String,
    description: String,
    structs: Vec<StructDef>,
    enums: Vec<EnumDef>,
}

#[derive(serde::Deserialize)]
struct EnumDef {
    name: String,
    arch: Option<Vec<String>>,
    description: String,
    typ: String,
    variants: Vec<VariantDef>,
}

#[derive(serde::Deserialize)]
struct VariantDef {
    name: String,
    arch: Option<Vec<String>>,
    description: String,
    value: u64,
}

/// Type-safe abstraction for indentation level
#[derive(Copy, Clone)]
struct Indentation(usize);

/// A CLI tool to automatically generate struct definitions in several languages
///
/// It generates a single file for each
#[derive(Parser)]
#[command(version, about)]
struct Cli {
    /// Path to the file to be read
    infile: PathBuf,
    /// Path to the file to be written
    #[arg(short, long)]
    outfile: PathBuf,
    /// Target language for `outfile`
    #[arg(short, long)]
    lang: String,
}

fn readfile(path: &PathBuf) -> String {
    let mut f = match File::open(path) {
        Ok(x) => x,
        Err(x) => {
            eprintln!("Can't read {}: {x}", path.to_string_lossy());
            std::process::exit(1);
        }
    };

    let mut ret = String::new();
    if let Err(x) = f.read_to_string(&mut ret) {
        eprintln!("IO error on {}: {x}", path.to_string_lossy());
        std::process::exit(1);
    };
    ret
}

fn main() {
    let cli = Cli::parse();

    let in_filedef: FileDef = match toml::from_str(&readfile(&cli.infile)) {
        Ok(x) => x,
        Err(x) => {
            eprintln!("{x}");
            std::process::exit(1);
        }
    };

    let output = match cli.lang.as_str() {
        "rust" => rs_lang::parse(&in_filedef),
        "c" => c_lang::parse(&in_filedef),
        _ => {
            eprintln!("Unknown lang: {}", cli.lang);
            std::process::exit(1);
        }
    };

    let mut f = match File::create(&cli.outfile) {
        Ok(x) => x,
        Err(x) => {
            eprintln!("Can't open {}: {x}", cli.outfile.to_string_lossy());
            std::process::exit(1);
        }
    };

    if let Err(x) = write!(f, "{output}") {
        eprintln!("Can't write to {}: {x}", cli.outfile.to_string_lossy());
        std::process::exit(1);
    }
}
