use std::{fmt::Write, str::FromStr};

use crate::{EnumDef, FieldDef, FileDef, Indentation, StructDef};

use convert_case::{Case, Casing};

/// Convert an IDL type into its Rust type.
fn fieldtype_rs(field: &FieldDef) -> String {
    if field.typ.starts_with('*') {
        // All pointers are represented by 64bits
        return String::from_str("u64").unwrap();
    }

    let typ = typ_rs(field.typ.as_str());

    if let Some(array_len) = field.array_len {
        return format!("[{typ};{array_len}]");
    }

    typ
}

/// Convert an IDL type into its Rust type.
fn typ_rs(typ: &str) -> String {
    String::from_str(match typ {
        "u8" => "u8",
        "u16" => "u16",
        "u32" => "u32",
        "u64" => "u64",
        "i8" => "i8",
        "i16" => "i16",
        "i32" => "i32",
        "i64" => "i64",
        "xenctl_bitmap" => "XenCtlBitmap",
        _ => {
            eprintln!("typ_rs - Unknown type: {typ}");
            std::process::exit(1);
        }
    })
    .unwrap()
}

/// Map a set of architectures into a conditional attribute
///
/// # Examples
///
/// ```
///     let arch = vec!["x86_64", "i386"];
///     let s = arch_to_cfg(arch);
///     assert_eq!(s, "#[cfg(any(target_arch = "x86_64", target_arch = "x86"))]")
/// ```
fn arch_to_cfg(arch: &[String]) -> String {
    let cfg: Vec<String> = arch
        .iter()
        .map(|x| format!("target_arch = \"{}\"", if x == "i386" { "x86" } else { x }))
        .collect();

    if cfg.len() == 1 {
        return format!("#[cfg({})]", cfg[0]);
    }
    format!("#[cfg(any({}))]", cfg.join(", "))
}

/// Add a comment to a struct or a field.
///
/// * `bars`: allows the choice of `//!` or `///`
/// * `comment`: Actual comment being written
/// * `ind`: provides the indentation level
fn comment(out: &mut String, bars: &str, comment: &str, ind: Indentation) {
    let spaces = " ".repeat(4 * ind.0);
    for line in comment.split('\n') {
        write!(out, "{spaces}{bars}").unwrap();
        if !line.is_empty() {
            write!(out, " {line}").unwrap();
        }
        writeln!(out).unwrap();
    }
}

/// Write a Rust-compatible struct onto `out`
fn structgen(out: &mut String, def: &StructDef) {
    comment(out, "///", &def.description, Indentation(0));
    writeln!(out, "#[repr(C)]").unwrap();
    if let Some(arch) = &def.arch {
        writeln!(out, "{}", arch_to_cfg(arch)).unwrap();
    }
    writeln!(out, "pub struct {} {{", def.name.to_case(Case::Pascal)).unwrap();
    for f in &def.fields {
        comment(out, "///", &f.description, Indentation(1));
        if let Some(arch) = &f.arch {
            writeln!(out, "    {}", arch_to_cfg(arch)).unwrap();
        }
        writeln!(
            out,
            "    {}: {},",
            f.name.to_case(Case::Snake),
            fieldtype_rs(f)
        )
        .unwrap();
    }
    writeln!(out, "}}\n").unwrap();
}

/// Write a Rust-compatible enum onto `out`
fn enumgen(out: &mut String, def: &EnumDef) {
    comment(out, "///", &def.description, Indentation(0));
    writeln!(out, "#[repr({})]", typ_rs(&def.typ)).unwrap();
    if let Some(arch) = &def.arch {
        writeln!(out, "{}", arch_to_cfg(arch)).unwrap();
    }
    writeln!(out, "pub enum {} {{", def.name.to_case(Case::Pascal)).unwrap();
    for f in &def.variants {
        comment(out, "///", &f.description, Indentation(1));
        if let Some(arch) = &f.arch {
            writeln!(out, "{}", arch_to_cfg(arch)).unwrap();
        }
        writeln!(out, "    {} = {},", f.name.to_case(Case::Pascal), f.value).unwrap();
    }
    writeln!(out, "}}\n").unwrap();
}

pub fn parse(filedef: &FileDef) -> String {
    let mut out = String::new();

    comment(&mut out, "//!", &filedef.description, Indentation(0));
    writeln!(out).unwrap();

    for def in &filedef.structs {
        structgen(&mut out, def);
    }
    for def in &filedef.enums {
        enumgen(&mut out, def);
    }

    out
}
