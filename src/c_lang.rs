use std::{fmt::Write, str::FromStr};

use crate::{EnumDef, FieldDef, FileDef, Indentation, StructDef};

use convert_case::{Case, Casing};

/// Convert an IDL type into its Rust type.
fn typ_c(typ: &str) -> String {
    String::from_str(match typ {
        "u8" => "uint8_t",
        "u16" => "uint16_t",
        "u32" => "uint32_t",
        "u64" => "uint64_t",
        "i8" => "int8_t",
        "i16" => "int16_t",
        "i32" => "int32_t",
        "i64" => "int64_t",
        "xenctl_bitmap" => "struct xenctl_bitmap",
        _ => {
            eprintln!("typ_c - Unknown type: {typ}");
            std::process::exit(1);
        }
    })
    .unwrap()
}

/// Convert an IDL type into its C type.
fn fieldtype_c(field: &FieldDef) -> String {
    if field.typ.starts_with('*') {
        let mut typ = typ_c(&field.typ[1..]);
        typ.insert_str(0, "XEN_GUEST_HANDLE_64(");
        typ.push(')');
        typ
    } else {
        typ_c(&field.typ)
    }
}

/// Add a comment to a struct or a field.
fn comment(out: &mut String, comment: &str, ind: Indentation) {
    let spaces = " ".repeat(4 * ind.0);

    if comment.contains('\n') {
        writeln!(out, "{spaces}/*").unwrap();
        for line in comment.split('\n') {
            write!(out, "{spaces} *").unwrap();
            if !line.is_empty() {
                write!(out, " {line}").unwrap();
            }
            writeln!(out).unwrap();
        }
        writeln!(out, "{spaces} */").unwrap();
    } else {
        writeln!(out, "{spaces}/* {comment} */").unwrap();
    }
}

/// Map a set of architectures into proper #if statement
///
/// # Examples
///
/// ```
///     let arch = vec!["x86_64", "i386"];
///     let s = arch_to_cfg(arch);
///     assert_eq!(s, "defined(__x86_64__) || defined(__i386__)");
/// ```
fn arch_to_cfg(arch: &[String]) -> String {
    let cfg: Vec<String> = arch.iter().map(|x| format!("defined(__{x}__)")).collect();
    cfg.join(" || ")
}

/// Write a C-compatible struct onto `out`
fn structgen(out: &mut String, def: &StructDef) {
    if let Some(arch) = &def.arch {
        writeln!(out, "#if {}", arch_to_cfg(arch)).unwrap();
    }
    comment(out, &def.description, Indentation(0));
    writeln!(out, "struct {} {{", def.name.to_case(Case::Snake)).unwrap();
    for f in &def.fields {
        if let Some(arch) = &f.arch {
            writeln!(out, "#if {}", arch_to_cfg(arch)).unwrap();
        }
        comment(out, &f.description, Indentation(1));

        write!(
            out,
            "    {} {}",
            fieldtype_c(f),
            f.name.to_case(Case::Snake)
        )
        .unwrap();

        if let Some(n) = f.array_len {
            write!(out, "[{n}]").unwrap();
        }

        writeln!(out, ";").unwrap();

        if let Some(arch) = &f.arch {
            writeln!(out, "#endif /* {} */", arch_to_cfg(arch)).unwrap();
        }
    }
    writeln!(out, "}};").unwrap();

    if let Some(arch) = &def.arch {
        writeln!(out, "#endif /* {} */", arch_to_cfg(arch)).unwrap();
    }

    writeln!(out).unwrap();
}

/// Write a C-compatible enum onto `out`
fn enumgen(out: &mut String, def: &EnumDef) {
    if let Some(arch) = &def.arch {
        writeln!(out, "#if {}", arch_to_cfg(arch)).unwrap();
    }
    comment(out, &def.description, Indentation(0));
    writeln!(out, "enum {} {{", def.name.to_case(Case::Snake)).unwrap();
    for f in &def.variants {
        if let Some(arch) = &f.arch {
            writeln!(out, "#if {}", arch_to_cfg(arch)).unwrap();
        }
        comment(out, &f.description, Indentation(1));
        writeln!(
            out,
            "    {}_{} = {},",
            def.name.to_case(Case::UpperSnake),
            f.name.to_case(Case::UpperSnake),
            f.value
        )
        .unwrap();
        if let Some(arch) = &f.arch {
            writeln!(out, "#endif /* {} */", arch_to_cfg(arch)).unwrap();
        }
    }
    writeln!(out, "}};").unwrap();

    if let Some(arch) = &def.arch {
        writeln!(out, "#endif /* {} */", arch_to_cfg(arch)).unwrap();
    }

    writeln!(out).unwrap();
}

pub fn parse(filedef: &FileDef) -> String {
    let mut out = String::new();
    let name = filedef.name.to_uppercase();

    comment(&mut out, &filedef.description, Indentation(0));
    writeln!(out, "#ifndef __XEN_AUTOGEN_{name}_H").unwrap();
    writeln!(out, "#define __XEN_AUTOGEN_{name}_H").unwrap();
    writeln!(out, "#include <stdint.h>\n").unwrap();

    for def in &filedef.structs {
        structgen(&mut out, def);
    }

    for def in &filedef.enums {
        enumgen(&mut out, def);
    }

    writeln!(out, "#endif /* __XEN_AUTOGEN_{name}_H */\n").unwrap();
    out
}
